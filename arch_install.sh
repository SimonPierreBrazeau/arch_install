git config --global user.name "Simon-Pierre Brazeau"
git config --global user.email "simon.pierre.brazeau@gmail.com"

ssh_config=~/.ssh/config
ssh_key=~/.ssh/gitlab
ssh-keygen -t ed25519 -f $ssh_key -C "Gitlab for $HOSTNAME" -N ""

touch $ssh_config
echo "Host gitlab.com" >> $ssh_config
echo "    PreferredAuthentications publickey" >> $ssh_config
echo "    IdentityFile $ssh_key" >> $ssh_config

echo Printing $ssh_key.pub. Please copy the output to your Gitlab SSH keys.
cat $ssh_key.pub
read -p "Press enter to continue." </dev/tty

if [ ! -d ~/Code ]; then
  mkdir ~/Code
fi

cd ~/Code
git clone git@gitlab.com:SimonPierreBrazeau/configs.git
